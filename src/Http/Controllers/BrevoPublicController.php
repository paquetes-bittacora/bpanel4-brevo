<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Brevo\Http\Controllers;

use Bittacora\Bpanel4\Brevo\Bpanel4Brevo;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Throwable;

final class BrevoPublicController
{
    public function __construct(
        private readonly Redirector $redirector,
        private readonly Bpanel4Brevo $brevo,
        private readonly ExceptionHandler $exceptionHandler,
    ) {
    }

    public function subscribe(Request $request): RedirectResponse
    {
        try {
            $this->brevo->addSubscriber($request->get('email'));
            return $this->redirector->back()->with(['alert-success' => '¡Gracias por suscribirse a nuestra newsletter!']);
        } catch (Throwable $t) {
            $message = 'Ocurrió un error al suscribirle a la newsletter';

            if ($this->clientHadAlreadyRegistered($t)) {
                $message = 'Ya se había registrado en la newsletter con esta dirección de correo.';
            } else {
                $this->exceptionHandler->report($t);
            }

            return $this->redirector->back()->with(['alert-danger' => $message]);
        }
    }

    private function clientHadAlreadyRegistered(Throwable|\Exception $t): bool
    {
        return method_exists($t, 'getResponseBody') &&
            in_array(json_decode($t->getResponseBody())->message, [
                'Contact already exist',
                'Unable to create contact, email is already associated with another Contact'
            ]);
    }
}
