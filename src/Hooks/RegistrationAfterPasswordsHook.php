<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Brevo\Hooks;

use Illuminate\Contracts\View\Factory;

final class RegistrationAfterPasswordsHook
{
    public function __construct(protected readonly Factory $view)
    {
    }

    public function handle(): void
    {
        echo $this->view->make('bpanel4-brevo::hooks.newsletter-checkbox-hook')->render();
    }
}