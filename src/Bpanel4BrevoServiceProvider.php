<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Brevo;

use Bittacora\Bpanel4\Brevo\Commands\InstallCommand;
use Bittacora\Bpanel4\Brevo\Hooks\RegistrationAfterPasswordsHook;
use Bittacora\Bpanel4\Brevo\Listeners\UserCreatedListener;
use Bittacora\Bpanel4\Clients\Events\UserCreated;
use Bittacora\Bpanel4\HooksComponent\Services\Hooks;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\ServiceProvider;

final class Bpanel4BrevoServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-brevo';

    public function boot(): void
    {
        $this->commands(InstallCommand::class);
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);
        $this->publishes([__DIR__.'/../config/bpanel4-brevo.php' => config_path('bpanel4-brevo.php')]);

        $this->registerHooks();
        $this->subscribeToEvents();
    }

    private function registerHooks(): void
    {
        $hooks = $this->app->get(Hooks::class);
        $registrationAfterPasswordsHook = $this->app->get(RegistrationAfterPasswordsHook::class);
        $hooks->register('registration-after-passwords', $registrationAfterPasswordsHook->handle(...));
    }

    private function subscribeToEvents(): void
    {
        /** @var Dispatcher $dispatcher */
        $dispatcher = $this->app->make(Dispatcher::class);
        $dispatcher->listen(
            UserCreated::class,
            [UserCreatedListener::class, 'handle']
        );
    }
}
