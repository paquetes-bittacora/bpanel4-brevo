<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Brevo\Listeners;

use Bittacora\Bpanel4\Brevo\Bpanel4Brevo;
use Bittacora\Bpanel4\Clients\Events\UserCreated;
use Brevo\Client\ApiException;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Throwable;

final class UserCreatedListener
{
    public function __construct(
        protected readonly Bpanel4Brevo $brevo,
        protected readonly ExceptionHandler $handler,
    ) {
    }

    /**
     * @throws ApiException
     */
    public function handle(UserCreated $userCreated): void
    {
        $subscribeToNewsletter = 1 === (int) ($userCreated->others['subscribe-to-brevo-newsletter'] ?? false);

        if (!$subscribeToNewsletter) {
            return;
        }

        try {
            $this->brevo->addSubscriber($userCreated->email);
        } catch (Throwable $t) {
            $this->handler->report($t);
        }
    }
}