<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Brevo\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    /** @var string */
    protected $signature = 'bpanel4-brevo:install';

    /** @var string */
    protected $description = 'Instala el paquete de suscripción a newsletters de Brevo';

    private const PERMISSIONS = ['edit', 'update'];

    public function handle(AdminMenu $adminMenu): void
    {
        $this->registerEnvVariables();
    }

    private function registerEnvVariables(): void
    {
        $env = App::environment();
        $file = in_array($env, ['production', 'local']) ? base_path() . '/.env' : base_path() . '/.env.' . $env;

        $fileContents = file_get_contents($file);

        // El siguiente bloque hay que dejarlo mal formateado para que salga bien en el .env
        $string = "\n# bPanel4 Brevo ------------------------------
BREVO_API_KEY=
BREVO_LIST_ID=
# /bPanel4 Brevo -----------------------------";

        if (!str_contains($fileContents, 'bPanel4 Brevo')) {
            file_put_contents($file, $string, FILE_APPEND);
        }

        $this->call('vendor:publish', ['--provider' => 'Bittacora\Bpanel4\Brevo\Bpanel4BrevoServiceProvider']);
    }
}
