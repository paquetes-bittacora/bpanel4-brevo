<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Brevo;

use Brevo\Client\Api\ContactsApi;
use Brevo\Client\ApiException;
use Brevo\Client\Configuration;
use Brevo\Client\Model\CreateContact;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Debug\ExceptionHandler;

final class Bpanel4Brevo
{
    public function __construct(
        private readonly Repository $config,
        private readonly ExceptionHandler $exceptionHandler,
    ) {
    }

    /**
     * @throws ApiException
     */
    public function addSubscriber(string $email): void
    {
        $apiKey = $this->config->get('bpanel4-brevo.api-key');
        $listId = $this->config->get('bpanel4-brevo.list-id');
        $config = Configuration::getDefaultConfiguration()
            ->setApiKey('api-key', $apiKey);

        $contactsApi = new ContactsApi(new Client(), $config);
        $contactsApi->createContact(new CreateContact([
            'email' => $email,
            'listIds' => [(int)$listId],
        ]));
    }
}