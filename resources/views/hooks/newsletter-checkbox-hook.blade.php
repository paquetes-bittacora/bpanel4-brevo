<div class="registration-newsletter-field">
    @livewire('form::input-checkbox', [
        'name' => 'subscribe-to-brevo-newsletter',
        'value' => 1,
        'checked' => false,
        'labelText' => __('bpanel4-brevo::public.subscribe-to-newsletter'),
        'bpanelForm' => true]
    )
</div>