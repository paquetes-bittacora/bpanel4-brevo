<form method="post" action="{{ route('bpanel4-brevo.subscribe') }}" class="brevo-form">
    <div class="inputs">
        <input type="email" name="email" placeholder="{{ __('Introduzca su email') }}">
        <input type="submit" value="{{ __('Suscribirse') }}">
        @if(isset($policyText))
            <div class="legal">
                <label><input type="checkbox" required> {!! __($policyText) !!}</label>
            </div>
        @endif
    </div>
    @csrf
</form>
