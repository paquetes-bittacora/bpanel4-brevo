<?php

declare(strict_types=1);

return [
    'subscribe-to-newsletter' => 'Deseo suscribirme a la newsletter.',
];