<?php

declare(strict_types=1);

return [
    'api-key' => env('BREVO_API_KEY', ''),
    'list-id' => env('BREVO_LIST_ID', ''),
    // Mostrar el check para suscribirse a la newsletter a la hora de registrarse.
    'show-newsletter-check-in-registration' => true,
];