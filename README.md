# bPanel4 Brevo

Paquete para mostrar un formulario de suscripción a una newsletter de Brevo en bPanel4. Basado
en [getbrevo/brevo-php](https://github.com/getbrevo/brevo-php)

## 💾 Instalación

El paquete se instala automáticamente, pero es necesario completar los siguientes valores en el `.env`:

```
BREVO_API_KEY=
BREVO_PARTNER_KEY=
```

## ⚙️ Configuración

En el archivo `config/bpanel4-brevo.php` se puede indicar si queremos que se muestre
una casilla para suscribirse desde el formulario de registro del paquete `bpanel4-clients`.
Para habilitar esta casilla, hay que poner `show-newsletter-check-in-registration` a true.
Este paquete se encargará automáticamente de procesar ese checkbox, no es necesario modificar
nada más.

## ✍️ Uso

Para incluir el formulario en la página, hay que llamar a:

```
@include('bpanel4-brevo::form', ['policyText' => 'Acepto las condiciones legales'])
```
