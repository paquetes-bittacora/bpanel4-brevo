<?php

declare(strict_types=1);

use Bittacora\Bpanel4\Brevo\Http\Controllers\BrevoPublicController;
use Illuminate\Support\Facades\Route;

Route::prefix('/brevo')->name('bpanel4-brevo.')->middleware(['web'])
    ->group(static function () {
        Route::post('/suscribir', [BrevoPublicController::class, 'subscribe'])->name('subscribe');
    });
